import java.util.*;

public class CentralTendencyCal {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++)
            arr[i] = sc.nextInt();
        int sum=0,mean;
        for(int i=0;i<arr.length;i++)
            sum+=arr[i];
        mean=sum/n;
        System.out.println("mean value is"+mean);
        Arrays.sort(arr);
        if(n%2!=0)
            System.out.println("median value is"+arr[n/2]);

        else
            System.out.println("median value is"+(arr[n/2]+arr[n/2-1])/2.0);
        HashMap<Integer, Integer> hm= new HashMap<>();
        for (int i=0;i<n;i++) {
            if (hm.containsKey(arr[i])) {
                hm.put(arr[i], hm.get(arr[i]) + 1);
            }
            else {
                hm.put(arr[i], 1);
            }
        }
        int maxValue=(Collections.max(hm.values()));
        for (Map.Entry et : hm.entrySet()) {
            if (et.getValue().equals(maxValue)) {
                System.out.println("mode value is"+et.getKey());
                break;
            }
        }



    }

    }
